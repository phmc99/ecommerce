import Form from "../../components/Form";
import Menu from "../../components/Menu";

const Finish = () => {
  return (
    <>
      <Menu inCart={true} />

      <Form />
    </>
  );
};

export default Finish;
