import "./style.scss";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { removeProductThunk } from "../../store/modules/cart/thunks";

const ProductInput = ({ id, image, name, price, quantity }) => {
  const [value, setValue] = useState(quantity);
  const dispatch = useDispatch();
  const cart = useSelector((state) => state.cart);

  const handleAdd = () => {
    setValue(value + 1);
    cart.map((item) => {
      if (item.id === id) {
        item.quantity++;
      }

      return item;
    });
  };

  const handleSub = () => {
    if (value !== 1) {
      setValue(value - 1);
      cart.map((item) => {
        if (item.id === id) {
          item.quantity--;
        }

        return item;
      });
    }
  };

  const removeItem = (id) => {
    const product = cart.find((item) => item.id === id);
    dispatch(removeProductThunk(product));
  };

  return (
    <div className="product-content">
      <img src={image} alt={name} />
      <div className="product-info">
        <h2>{name}</h2>
        <span>R${price}</span>
        <span className="remove-product" onClick={() => removeItem(id)}>
          Remover do carrinho
        </span>
      </div>
      <div className="button-container">
        <button onClick={handleSub} disabled={value === 1}>
          -
        </button>
        <input type="number" value={value} />
        <button onClick={handleAdd}>+</button>
      </div>
    </div>
  );
};

export default ProductInput;
