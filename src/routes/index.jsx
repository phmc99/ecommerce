import { Route, Switch } from "react-router-dom";
import Cart from "../pages/CartPage";
import Finish from "../pages/FinishPage";
import Home from "../pages/HomePage";
import Confirm from "../pages/ConfirmPage";
import Success from "../pages/SuccessPage";

const Routes = () => {
  return (
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route path="/cart">
        <Cart />
      </Route>
      <Route path="/finish">
        <Finish />
      </Route>
      <Route path="/confirm">
        <Confirm />
      </Route>
      <Route path="/success">
        <Success />
      </Route>
    </Switch>
  );
};

export default Routes;
