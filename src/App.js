import { Toaster } from "react-hot-toast";
import Routes from "./routes";
import "./style/global.scss";

const App = () => {
  return (
    <>
      <Toaster />
      <Routes />
    </>
  );
};

export default App;
