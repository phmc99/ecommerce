import { useHistory } from "react-router-dom";
import illustration from "../../assets/illustration.svg";
import Button from "../../components/Button";
import "./style.scss";
import Confetti from "react-confetti";
import { resetCart } from "../../store/modules/cart/actions";
import { useDispatch } from "react-redux";

const Success = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  const handleClick = () => {
    dispatch(resetCart());
    history.push("/");
  };

  return (
    <>
      <Confetti recycle={false} />
      <div className="success-page">
        <h1>Agradecemos a sua compra!</h1>
        <img src={illustration} alt="Ilustração" />
        <Button func={handleClick}>Voltar ao início</Button>
      </div>
    </>
  );
};

export default Success;
