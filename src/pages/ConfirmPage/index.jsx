import OrderDetails from "../../components/OrderDetails";
import Button from "../../components/Button";
import "./style.scss";
import { useSelector } from "react-redux";
import Menu from "../../components/Menu";
import api from "../../services/api";
import { useHistory } from "react-router-dom";
import toast from "react-hot-toast";

const Confirm = () => {
  const cart = useSelector((state) => state.cart);
  const history = useHistory("/success");

  const total = cart
    .reduce((acc, item) => acc + Number(item.price) * Number(item.quantity), 0)
    .toFixed(2);

  const handleClick = () => {
    const order = JSON.parse(localStorage.getItem("order"));
    const body = {
      produtos: order,
      total: total,
    };
    api
      .post("/compras/", body)
      .then(toast.success("Pedido realizado com sucesso"));

    localStorage.clear();
    history.push("/success");
  };

  return (
    <>
      <Menu inCart={true} />
      <div className="confirm-page">
        <div className="header">
          <h1>Confirme seu pedido</h1>
          <span>Aqui estão os detalhes do seu pedido</span>
        </div>

        <div className="order-content">
          <ul>
            {cart.map((item, index) => (
              <OrderDetails
                quantity={item.quantity}
                name={item.name}
                price={item.price}
                key={index}
              />
            ))}
          </ul>
          <span className="total-price">
            Total: R$
            {total}
          </span>
        </div>
        <Button func={handleClick}>Confirmar compra</Button>
      </div>
    </>
  );
};

export default Confirm;
