const product = (id, image, name, price) => ({
  id,
  image,
  name,
  price,
  quantity: 1,
});

export default product;
