import { AppBar, IconButton, Input, Toolbar } from "@material-ui/core";
import { BiArrowBack, BiCart } from "react-icons/bi";
import useStyles from "./style";
import "./style.scss";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
import { useState } from "react";

const Menu = ({ inCart = false, products, setProducts, productsAux }) => {
  const classes = useStyles();
  const history = useHistory();
  const [userInput, setUserInput] = useState("");

  const cart = useSelector((state) => state.cart);

  const filterProducts = (e) => {
    setUserInput(e.target.value);
    if (userInput.trim() !== "") {
      setProducts(products.filter((item) => item.nome.includes(userInput)));
    }
    if (userInput.trim() === "") {
      setProducts(productsAux);
    }
  };

  const handleCartPage = () => {
    history.push("/cart");
  };

  const handlePreviousPage = () => {
    history.goBack();
  };

  return (
    <AppBar position="static" className={classes.root}>
      <Toolbar>
        {inCart ? (
          <IconButton onClick={handlePreviousPage}>
            <BiArrowBack />
          </IconButton>
        ) : (
          <>
            <Input
              type="search"
              placeholder="Pesquise"
              value={userInput}
              onChange={filterProducts}
              className={classes.root}
            />
            <IconButton onClick={handleCartPage}>
              {cart.length > 0 && (
                <span className="cart-products">{cart.length}</span>
              )}
              <BiCart />
            </IconButton>
          </>
        )}
      </Toolbar>
    </AppBar>
  );
};

export default Menu;
