import { useEffect, useState } from "react";
import Menu from "../../components/Menu";
import ProductCard from "../../components/ProductCard";
import api from "../../services/api";
import "./style.scss";

const Home = () => {
  const [products, setProducts] = useState([]);
  const [productsAux, setProductsAux] = useState([]);

  useEffect(() => {
    api.get("/produtos/").then((response) => {
      setProducts(response.data);
      setProductsAux(response.data);
    });
  }, []);

  return (
    <>
      <Menu
        products={products}
        setProducts={setProducts}
        productsAux={productsAux}
      />
      <div className="home-container">
        {products.map((item, index) => (
          <ProductCard
            id={item["id_produto"]}
            image={item.imagem}
            name={item.nome}
            price={item.preco}
            key={index}
          />
        ))}
      </div>
    </>
  );
};

export default Home;
