import { ADD_TO_CART, REMOVE_FROM_CART, RESET_CART } from "./actionTypes";

export const addProduct = (product) => ({
  type: ADD_TO_CART,
  product,
});

export const removeProduct = (product) => ({
  type: REMOVE_FROM_CART,
  product,
});

export const resetCart = () => ({
  type: RESET_CART,
});
