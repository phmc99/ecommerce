import toast from "react-hot-toast";
import { useDispatch, useSelector } from "react-redux";
import product from "../../models/product";
import { addProductThunk } from "../../store/modules/cart/thunks";
import Button from "../Button";
import "./style.scss";

const ProductCard = ({ id, image, name, price }) => {
  const dispatch = useDispatch();
  const cart = useSelector((state) => state.cart);

  const handleClick = () => {
    const currentItem = cart.find((item) => item.id === id);
    if (cart.includes(currentItem)) {
      toast.error("Esse produto já foi adicionado", { duration: 1000 });
    } else {
      dispatch(addProductThunk(product(id, image, name, price)));
      toast.success("Produto adicionado ao carrinho", { duration: 1000 });
    }
  };

  return (
    <div className="ProductCardBox">
      <img src={image} alt={name} />
      <h2>{name}</h2>
      <span>R${price}</span>
      <Button func={handleClick}>Adicionar</Button>
    </div>
  );
};

export default ProductCard;
