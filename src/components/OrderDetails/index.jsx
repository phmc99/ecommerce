import "./style.scss";

const OrderDetails = ({ quantity, name, price }) => {
  return (
    <li>
      <div className="info-index">
        <span>{quantity}</span>
        <span>{name}</span>
      </div>
      <span>R${price}</span>
    </li>
  );
};

export default OrderDetails;
